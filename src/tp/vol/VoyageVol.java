package tp.vol;

public class VoyageVol{
	
	private int ordre;
	private Voyage voyagevol;
	private Vol volvoyage;
	
	public VoyageVol() {
		
	}
	
	
	public VoyageVol(Voyage voyagevol, Vol volvoyage) {
		super();
		this.voyagevol = voyagevol;
		this.volvoyage = volvoyage;
	}
	public int getOrdre() {
		return ordre;
	}
	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}
	public Voyage getVoyagevol() {
		return voyagevol;
	}
	public void setVoyagevol(Voyage voyagevol) {
		this.voyagevol = voyagevol;
	}
	public Vol getVolvoyage() {
		return volvoyage;
	}
	public void setVolvoyage(Vol volvoyage) {
		this.volvoyage = volvoyage;
	}



}
