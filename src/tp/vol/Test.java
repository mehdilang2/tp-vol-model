package tp.vol;

import java.util.Date;

public class Test {

	public static void main(String[] args) {
		
		
		Ville barcelone = new Ville();
		Ville paris = new Ville();
		Ville newyork = new Ville();
		
		barcelone.setNom("Barcelone");
		paris.setNom("Paris");
		newyork.setNom("New-York");
		
		Aeroport barceloneelprat = new Aeroport();
		Aeroport pariscdg = new Aeroport();
		Aeroport nyjk = new Aeroport();
		
		barceloneelprat.setCode("BEP");
		pariscdg.setCode("CDG");
		nyjk.setCode("JFK");
		
		Vol parisbarcelone18nov = new Vol();
		Vol parisbarcelone21nov = new Vol();
		Vol parisny18nov = new Vol();
		Vol parisny23nov = new Vol();
		Vol barceloneny19nov = new Vol();
		Vol barceloneny24nov = new Vol();
		Vol barceloneparis21nov = new Vol();
		Vol barceloneparis24nov = new Vol();
		Vol nyparis23dec = new Vol();
		Vol nyparis19dec = new Vol();
		Vol nybarcelone18oct = new Vol();
		Vol nybarcelone16oct = new Vol();
		
		
		parisbarcelone18nov.setDateDepart(new Date(2019,10,18));
		parisbarcelone18nov.setDateArrivee(new Date(2019,10,18));
		parisbarcelone18nov.setNumero(181100);
		parisbarcelone18nov.setOuvert(true);
		parisbarcelone18nov.setNbPlaces(18);
		
		parisbarcelone21nov.setDateDepart(new Date(2019,10,21));
		parisbarcelone21nov.setDateArrivee(new Date(2019,10,21));
		parisbarcelone21nov.setNumero(211100);
		parisbarcelone21nov.setOuvert(true);
		parisbarcelone21nov.setNbPlaces(24);
		
		parisny18nov.setDateDepart(new Date(2019,10,18));
		parisny18nov.setDateArrivee(new Date(2019,10,18));
		parisny18nov.setNumero(181101);
		parisny18nov.setOuvert(true);
		parisny18nov.setNbPlaces(28);
		
		barceloneny19nov.setDateDepart(new Date(2019,10,19));
		barceloneny19nov.setDateArrivee(new Date(2019,10,19));
		barceloneny19nov.setNumero(191100);
		barceloneny19nov.setOuvert(true);
		barceloneny19nov.setNbPlaces(21);
		
		barceloneny24nov.setDateDepart(new Date(2019,10,24));
		barceloneny24nov.setDateArrivee(new Date(2019,10,24));
		barceloneny24nov.setNumero(241100);
		barceloneny24nov.setOuvert(true);
		barceloneny24nov.setNbPlaces(32);
		
		nyparis23dec.setDateDepart(new Date(2019,11,23));
		nyparis23dec.setDateArrivee(new Date(2019,11,23));
		nyparis23dec.setNumero(231200);
		nyparis23dec.setOuvert(true);
		nyparis23dec.setNbPlaces(32);
		
		nyparis19dec.setDateDepart(new Date(2019,11,19));
		nyparis19dec.setDateArrivee(new Date(2019,11,19));
		nyparis19dec.setNumero(191101);
		nyparis19dec.setOuvert(true);
		nyparis19dec.setNbPlaces(36);
		
		nybarcelone18oct.setDateDepart(new Date(2019,9,18));
		nybarcelone18oct.setDateArrivee(new Date(2019,9,18));
		nybarcelone18oct.setNumero(181000);
		nybarcelone18oct.setOuvert(true);
		nybarcelone18oct.setNbPlaces(36);
		
		nybarcelone16oct.setDateDepart(new Date(2019,9,16));
		nybarcelone16oct.setDateArrivee(new Date(2019,9,16));
		nybarcelone16oct.setNumero(161000);
		nybarcelone16oct.setOuvert(true);
		nybarcelone16oct.setNbPlaces(36);
		
		
		
		
		Compagnie airfrance = new Compagnie();
		Compagnie britishairways = new Compagnie();
		
		
		
		
		
		barcelone.getAeroports().add(barceloneelprat);
		paris.getAeroports().add(pariscdg);
		newyork.getAeroports().add(nyjk);
		
		barceloneelprat.getVilles().add(barcelone);
		pariscdg.getVilles().add(paris);
		nyjk.getVilles().add(newyork);

		barceloneelprat.getVolsdeparts().add(barceloneny19nov);
		barceloneelprat.getVolsdeparts().add(barceloneparis21nov);
		pariscdg.getVolsdeparts().add(parisbarcelone18nov);
		pariscdg.getVolsdeparts().add(parisny18nov);
		nyjk.getVolsdeparts().add(nyparis23dec);
		nyjk.getVolsdeparts().add(nybarcelone18oct);
		barceloneelprat.getVolsdeparts().add(barceloneny24nov);
		barceloneelprat.getVolsdeparts().add(barceloneparis24nov);
		pariscdg.getVolsdeparts().add(parisbarcelone21nov);
		pariscdg.getVolsdeparts().add(parisny23nov);
		nyjk.getVolsdeparts().add(nyparis19dec);
		nyjk.getVolsdeparts().add(nybarcelone16oct);
		
		barceloneelprat.getVolsarrivees().add(nybarcelone16oct);
		barceloneelprat.getVolsarrivees().add(parisbarcelone18nov);
		pariscdg.getVolsarrivees().add(barceloneparis21nov);
		pariscdg.getVolsarrivees().add(parisny18nov);
		nyjk.getVolsarrivees().add(nyparis23dec);
		nyjk.getVolsarrivees().add(barceloneny19nov);
		barceloneelprat.getVolsarrivees().add(nybarcelone18oct);
		barceloneelprat.getVolsarrivees().add(parisbarcelone21nov);
		pariscdg.getVolsarrivees().add(barceloneparis24nov);
		pariscdg.getVolsarrivees().add(nyparis19dec);
		nyjk.getVolsarrivees().add(parisny23nov);
		nyjk.getVolsarrivees().add(barceloneny24nov);
		
		parisbarcelone18nov.setCompagnie(britishairways);
		parisbarcelone21nov.setCompagnie(airfrance);
		parisny18nov.setCompagnie(britishairways);
		parisny23nov.setCompagnie(airfrance);
		barceloneny19nov.setCompagnie(britishairways);
		barceloneny24nov.setCompagnie(airfrance);
		barceloneparis21nov.setCompagnie(britishairways);
		barceloneparis24nov.setCompagnie(airfrance);
		nyparis23dec.setCompagnie(britishairways);
		nyparis19dec.setCompagnie(airfrance);
		nybarcelone18oct.setCompagnie(britishairways);
		nybarcelone16oct.setCompagnie(airfrance);
		
		
		nybarcelone18oct.setNom("NY Barcelone 18 Octobre 2019");
		
		airfrance.setNomCompagnie("AirFrance");
		britishairways.setNomCompagnie("British Airways");
		
		ClientParticulier pauldoumer = new ClientParticulier();
		pauldoumer.setMail("paul.doumer@gmail.com");
		pauldoumer.setTelephone("06 67 85 96 78");
		pauldoumer.setMoyenPaiement("Carte Bleue");
		pauldoumer.setCivilite("M.");
		pauldoumer.setNom("Doumer");
		pauldoumer.setPrenoms("Paul Jacques");
		
		Adresse adressepauldoumer = new Adresse();
		adressepauldoumer.setVoie("48 Cours du medoc");
		adressepauldoumer.setVille("Limoges");
		adressepauldoumer.setCodePostal("87000");
		adressepauldoumer.setPays("France");
		
		pauldoumer.setAdressefacturation(adressepauldoumer);
		
		Passager mariedoumer = new Passager();
		mariedoumer.setMail("paul.doumer@gmail.com");
		mariedoumer.setTelephone("06 67 85 96 78");
		mariedoumer.setNom("Doumer");
		mariedoumer.setPrenoms("Marie Michelle");
		mariedoumer.setDtNaissance(new Date(1985,10,8));
		mariedoumer.setNumIdentite("45506878AEFR");
		mariedoumer.setNationalite("Franšaise");
		mariedoumer.setCivilite("Mme");
		mariedoumer.setTypePI("Carte identitÚ");
		mariedoumer.setDateValiditePI(new Date(2024,8,8));
		
		Adresse adressemariedoumer = new Adresse();
		adressemariedoumer.setVoie("48 Cours du medoc");
		adressemariedoumer.setVille("Limoges");
		adressemariedoumer.setCodePostal("87000");
		adressemariedoumer.setPays("France");
		
		mariedoumer.setAdresseprincipale(adressemariedoumer);
		
		
		
		Reservation reservation01 = new Reservation();
		reservation01.setClient(pauldoumer);
		reservation01.setDateReservation(new Date (2019,10,8));
		reservation01.setNumero("RES01A4U8");
		reservation01.setPassager(mariedoumer);
		reservation01.setStatut(true);
		reservation01.setTauxTVA(0.2f);
		
		mariedoumer.getReservationspassager().add(reservation01);
		
		Voyage parisnyvoyage = new Voyage();
		reservation01.setVoyage(parisnyvoyage);
		
		VoyageVol voyagevol = new VoyageVol(parisnyvoyage, parisbarcelone18nov);
		VoyageVol voyagevol2 = new VoyageVol(parisnyvoyage, nybarcelone18oct);
		
		parisnyvoyage.getVolsvoyage().add(voyagevol);
		parisnyvoyage.getVolsvoyage().add(voyagevol2);
		
		voyagevol.setOrdre(1);
		voyagevol2.setOrdre(2);
		
		parisbarcelone18nov.getVoyages().add(voyagevol);
		nybarcelone18oct.getVoyages().add(voyagevol2);
		

		
		
		System.out.println("Le pays de facturation du client de " + mariedoumer.getPrenoms() + " " + mariedoumer.getNom() + " est " + mariedoumer.getReservationspassager().get(0).getClient().getAdressefacturation().getPays());
		System.out.println("L'adresse de "+mariedoumer.getPrenoms() + " " + mariedoumer.getNom() + " est " + mariedoumer.getAdresseprincipale().getVoie() + " "+ mariedoumer.getAdresseprincipale().getVille() + " " + mariedoumer.getAdresseprincipale().getCodePostal() + " " + mariedoumer.getAdresseprincipale().getPays());
		
		if(mariedoumer.getReservationspassager().get(0).getClient() instanceof ClientParticulier) {
			ClientParticulier client = (ClientParticulier) mariedoumer.getReservationspassager().get(0).getClient();
			System.out.println(client.getPrenoms());
			System.out.println(client.getNom());

		
		}
		
		
		
	}

}
