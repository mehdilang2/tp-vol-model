package tp.vol;

import java.util.ArrayList;

public class Compagnie {
	
	private String nomCompagnie;
	private ArrayList<Vol> volscompagnie = new ArrayList<Vol>();

	public ArrayList<Vol> getVolscompagnie() {
		return volscompagnie;
	}

	public void setVolscompagnie(ArrayList<Vol> volscompagnie) {
		this.volscompagnie = volscompagnie;
	}

	public String getNomCompagnie() {
		return nomCompagnie;
	}

	public void setNomCompagnie(String nomCompagnie) {
		this.nomCompagnie = nomCompagnie;
	}

}
