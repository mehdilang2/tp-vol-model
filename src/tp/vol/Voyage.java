package tp.vol;

import java.util.ArrayList;

public class Voyage {
	
	private ArrayList<VoyageVol> volsvoyage = new ArrayList<VoyageVol>();
	private ArrayList<Reservation> reservationsvoyage = new ArrayList<Reservation>();
	

	public ArrayList<Reservation> getReservationsvoyage() {
		return reservationsvoyage;
	}

	public void setReservationsvoyage(ArrayList<Reservation> reservationsvoyage) {
		this.reservationsvoyage = reservationsvoyage;
	}

	public ArrayList<VoyageVol> getVolsvoyage() {
		return volsvoyage;
	}

	public void setVolsvoyage(ArrayList<VoyageVol> volsvoyage) {
		this.volsvoyage = volsvoyage;
	}


}
