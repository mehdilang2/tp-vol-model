package tp.vol;

import java.util.ArrayList;

public class Aeroport {
	
	private String code;
	private ArrayList<Ville> villes = new ArrayList<Ville>();
	private ArrayList<Vol> volsdeparts = new ArrayList<Vol>();
	private ArrayList<Vol> volsarrivees = new ArrayList<Vol>();

	public ArrayList<Vol> getVolsdeparts() {
		return volsdeparts;
	}

	public void setVolsdeparts(ArrayList<Vol> volsdeparts) {
		this.volsdeparts = volsdeparts;
	}

	public ArrayList<Vol> getVolsarrivees() {
		return volsarrivees;
	}

	public void setVolsarrivees(ArrayList<Vol> volsarrivees) {
		this.volsarrivees = volsarrivees;
	}

	public ArrayList<Ville> getVilles() {
		return villes;
	}

	public void setVilles(ArrayList<Ville> villes) {
		this.villes = villes;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	

}
