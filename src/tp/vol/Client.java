package tp.vol;

import java.util.ArrayList;

public abstract class Client extends Personne{
	
	private String moyenPaiement;
	private ArrayList<Reservation> reservationsclient = new ArrayList<Reservation>();
	private Adresse adressefacturation;
	

	public Adresse getAdressefacturation() {
		return adressefacturation;
	}

	public void setAdressefacturation(Adresse adressefacturation) {
		this.adressefacturation = adressefacturation;
	}

	public ArrayList<Reservation> getReservationsclient() {
		return reservationsclient;
	}

	public void setReservationsclient(ArrayList<Reservation> reservationsclient) {
		this.reservationsclient = reservationsclient;
	}

	public String getMoyenPaiement() {
		return moyenPaiement;
	}

	public void setMoyenPaiement(String moyenPaiement) {
		this.moyenPaiement = moyenPaiement;
	}
	

}
