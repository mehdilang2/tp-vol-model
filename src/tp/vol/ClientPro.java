package tp.vol;

public class ClientPro extends Client{
	
	private int numeroSiret;
	private String nomEntreprise;
	private String numTVA;
	private String typeEntreprise;
	
	public int getNumeroSiret() {
		return numeroSiret;
	}
	public void setNumeroSiret(int numeroSiret) {
		this.numeroSiret = numeroSiret;
	}
	public String getNomEntreprise() {
		return nomEntreprise;
	}
	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}
	public String getNumTVA() {
		return numTVA;
	}
	public void setNumTVA(String numTVA) {
		this.numTVA = numTVA;
	}
	public String getTypeEntreprise() {
		return typeEntreprise;
	}
	public void setTypeEntreprise(String typeEntreprise) {
		this.typeEntreprise = typeEntreprise;
	}
	
	

}
