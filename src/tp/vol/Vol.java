package tp.vol;

import java.util.ArrayList;
import java.util.Date;

public class Vol {
	
	private String nom;
	private Date dateDepart;
	private int numero;
	private Date dateArrivee;
	private boolean ouvert;
	private int nbPlaces;
	private ArrayList<Aeroport> aeroportsdeparts = new ArrayList<Aeroport>();
	private ArrayList<Aeroport> aeroportsarrivees = new ArrayList<Aeroport>();
	private Compagnie compagnie;
	private ArrayList<VoyageVol> voyages = new ArrayList<VoyageVol>();
	

	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public ArrayList<VoyageVol> getVoyages() {
		return voyages;
	}
	public void setVoyages(ArrayList<VoyageVol> voyages) {
		this.voyages = voyages;
	}
	public Compagnie getCompagnie() {
		return compagnie;
	}
	public void setCompagnie(Compagnie compagnie) {
		this.compagnie = compagnie;
	}
	public ArrayList<Aeroport> getAeroportsdeparts() {
		return aeroportsdeparts;
	}
	public void setAeroportsdeparts(ArrayList<Aeroport> aeroportsdeparts) {
		this.aeroportsdeparts = aeroportsdeparts;
	}
	public ArrayList<Aeroport> getAeroportsarrivees() {
		return aeroportsarrivees;
	}
	public void setAeroportsarrivees(ArrayList<Aeroport> aeroportsarrivees) {
		this.aeroportsarrivees = aeroportsarrivees;
	}
	public Date getDateDepart() {
		return dateDepart;
	}
	public void setDateDepart(Date dateDepart) {
		this.dateDepart = dateDepart;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public Date getDateArrivee() {
		return dateArrivee;
	}
	public void setDateArrivee(Date dateArrivee) {
		this.dateArrivee = dateArrivee;
	}
	public boolean isOuvert() {
		return ouvert;
	}
	public void setOuvert(boolean ouvert) {
		this.ouvert = ouvert;
	}
	public int getNbPlaces() {
		return nbPlaces;
	}
	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}

}
